package org.amdatu.kafka.test;

import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createFactoryConfiguration;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.amdatu.kafka.KafkaProducerService;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import junit.framework.TestCase;

public class KafkaProducerServiceTest extends TestCase {
	
	private static final String PRODUCER_PID = "org.amdatu.kafka.producer";
	private static final String BOOTSTRAP_SERVERS = "bootstrap.servers";
	
	private volatile KafkaProducerService kafkaProducerService;
	
	private CountDownLatch latch;

	public void setUp() throws Exception {
		super.setUp();
		
		configure(this)
		.add(createFactoryConfiguration(PRODUCER_PID).set(BOOTSTRAP_SERVERS, "localhost:9092"))
		.add(createServiceDependency().setService(KafkaProducerService.class).setRequired(true))
		.apply();		
	}
	
	public void tearDown() throws Exception {
		cleanUp(this);
		super.tearDown();
	}
	

    public void testProduce() throws InterruptedException  {
    	latch = new CountDownLatch(2);
    	Producer<byte[], byte[]> producer = kafkaProducerService.getProducer();
    	producer.send(new ProducerRecord<byte[], byte[]>("org.amdatu.kafka.test", "This is a test message.".getBytes()), new SendCallback(latch));
    	latch.countDown();
    	assertTrue(latch.await(5, TimeUnit.SECONDS));
    }
        
    private class SendCallback implements Callback {
    	private CountDownLatch m_latch;

    	public SendCallback(CountDownLatch latch) {
    		m_latch = latch;
		}

		@Override
		public void onCompletion(RecordMetadata arg0, Exception arg1) {
			m_latch.countDown();
			
		}    	
    }
    
}