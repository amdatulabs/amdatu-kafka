package org.amdatu.kafka.impl;

import java.util.Dictionary;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import org.amdatu.kafka.KafkaConsumerService;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.log.LogService;

public class KafkaConsumerServiceFactory implements ManagedServiceFactory {
	
	public static final String PID = "org.amdatu.kafka.consumer";
	private static final String ZOOKEEPER_CONNECT = "zookeeper.connect";
	private static final String GROUP_ID = "group.id";
	
	private final Map<String, Component> m_components = new ConcurrentHashMap<>();
	private volatile LogService m_logService;
	private volatile DependencyManager m_dm;

	@Override
	public String getName() {
		return PID;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void updated(String pid, Dictionary properties) throws ConfigurationException {
		remove(pid);
		
		String zookeeperConnect = getProperty(properties, ZOOKEEPER_CONNECT, "localhost:2181");
		String groupId = getProperty(properties, GROUP_ID, "consumer.group.id");
		
		ToggleServiceDependency toggleServiceDependency = new ToggleServiceDependency();
		KafkaConsumerServiceImpl instance = new KafkaConsumerServiceImpl(zookeeperConnect, groupId, toggleServiceDependency);
		
		Properties serviceProperties = new Properties();
		serviceProperties.put(ZOOKEEPER_CONNECT, zookeeperConnect);
		serviceProperties.put(GROUP_ID, groupId);
		Component component = m_dm.createComponent()
				.setInterface(KafkaConsumerService.class.getName(), serviceProperties)
				.setImplementation(instance);
		component.add(m_dm.createServiceDependency().setService(LogService.class).setRequired(false));

		// make sure the component is activated only when the consumer is actually connected to zookeeper
		component.add(toggleServiceDependency);
		
		m_dm.add(component);
		m_components.put(pid, component);
		m_logService.log(LogService.LOG_INFO, "Created KafkaConsumer service for pid '" + pid + "'");
	}

	@Override
	public void deleted(String pid) {
		remove(pid);
	}
	
	private void remove(String pid) {
		if(m_components.containsKey(pid)) {
			Component component = m_components.get(pid);
			if(component != null) {
				((KafkaConsumerServiceImpl)component.getInstance()).stop();
				m_dm.remove(component);
			}
			m_components.remove(pid);
			m_logService.log(LogService.LOG_INFO, "Removed KafkaConsumer service for pid '" + pid + "'");
			
		}
	}
	
	private String getProperty(Dictionary<String, String> properties, String key, String defaultValue) {
        String value = properties.get(key);
        if (value == null) {
            return defaultValue;
        }
        else {
            return value;
        }
	}

}
