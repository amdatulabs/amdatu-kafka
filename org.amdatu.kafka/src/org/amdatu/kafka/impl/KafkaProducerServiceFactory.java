package org.amdatu.kafka.impl;

import java.util.Dictionary;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import org.amdatu.kafka.KafkaProducerService;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.log.LogService;

public class KafkaProducerServiceFactory implements ManagedServiceFactory {
	
	public static final String PID = "org.amdatu.kafka.producer";
	private static final String BOOTSTRAP_SERVERS = "bootstrap.servers";
	
	private final Map<String, Component> m_components = new ConcurrentHashMap<>();
	private volatile LogService m_logService;
	private volatile DependencyManager m_dm;

	@Override
	public String getName() {
		return PID;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void updated(String pid, Dictionary properties) throws ConfigurationException {
		remove(pid);

		String boostrapServers = getProperty(properties, BOOTSTRAP_SERVERS, "localhost:9092");
		KafkaProducerServiceImpl instance = new KafkaProducerServiceImpl(boostrapServers);
		
		Properties serviceProperties = new Properties();
		serviceProperties.put(BOOTSTRAP_SERVERS, boostrapServers);
		Component component = m_dm.createComponent()
				.setInterface(KafkaProducerService.class.getName(), serviceProperties)
				.setImplementation(instance);
		m_dm.add(component);
		m_components.put(pid, component);
		m_logService.log(LogService.LOG_INFO, "Created KafkaProducer service for pid '" + pid + "'");
	}

	@Override
	public void deleted(String pid) {
		remove(pid);
	}
	
	private void remove(String pid) {
		if(m_components.containsKey(pid)) {
			Component component = m_components.get(pid);
			if(component != null){
				((KafkaProducerServiceImpl)component.getInstance()).close();
				m_dm.remove(component);
			}
			m_components.remove(pid);
			m_logService.log(LogService.LOG_INFO, "Removed KafkaProducer service for pid '" + pid + "'");			
		}
	}
	
    private String getProperty(Dictionary<String, String> properties, String key, String defaultValue) {
        String value = properties.get(key);
        if (value == null) {
            return defaultValue;
        }
        else {
            return value;
        }
    }    
}
