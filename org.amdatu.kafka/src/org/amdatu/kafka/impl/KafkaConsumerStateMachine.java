package org.amdatu.kafka.impl;

import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import kafka.consumer.ConsumerConfig;
import kafka.javaapi.consumer.ConsumerConnector;

public class KafkaConsumerStateMachine implements Runnable {
	
	enum State {
		NOT_CONNECTED,
		WAIT_FOR_CONNECT_TO_BEGIN,
		CONNECTED
	}
	
	enum QueueEvent {
		START,
		STOP,
		WAIT_FOR_CONNECT_FINISHED,
		DISCONNECT
	}
	
	private static final String ZOOKEEPER_CONNECT = "zookeeper.connect";
	private static final String GROUP_ID = "group.id";
	private static final String AUTO_COMMIT_INTERVAL = "auto.commit.interval.ms";
	
	private final String m_zookeeperConnect;
	private final String m_groupId;
	
	private final BlockingQueue<QueueEvent> m_queue;
	private State m_state = State.NOT_CONNECTED;
	
	private ExecutorService m_waitForConnectToBeginExecutor;	
	private ConsumerConnector m_consumerConnector;
	private final ToggleServiceDependency m_toggleServiceDependency;

	public KafkaConsumerStateMachine(String zookeeperConnect, String groupId, 
			ToggleServiceDependency toggleServiceDependency, BlockingQueue<QueueEvent> queue) {
		m_zookeeperConnect = zookeeperConnect;
		m_groupId = groupId;
		m_toggleServiceDependency = toggleServiceDependency;
		m_queue = queue;
	}
	
	public ConsumerConnector getConsumerConnector() {
		return m_consumerConnector;
	}
	
	@Override
	public void run() {		
		try {
			while(true) {
				processEvent(m_queue.take());
			}
		} catch (InterruptedException e) {
			System.err.println("Interrupted while reading the Kafka consumer state machine queue!");
		} finally {
			shutdownExecutors();
			m_state = State.NOT_CONNECTED;
		}
	}

	private void processEvent(QueueEvent event) {
		switch(event) {
		case START:
			processStart(); break;
		case STOP:
			processStop(); break;
		case WAIT_FOR_CONNECT_FINISHED:
			processWaitForConnectFinished(); break;
		case DISCONNECT:
			processDisconnect(); break;
		default: break;
		}
	}
	protected void processStart() {
		if(m_state == State.NOT_CONNECTED) {
			startWaitForConnectToBegin(0);
		} else {
			System.out.println("Kafka consumer state " + m_state.toString() + ", ignoring START");
		}
	}
	
	protected void processStop() {
		m_toggleServiceDependency.activate(false);
		shutdownExecutors();		
		m_state = State.NOT_CONNECTED;
	}

	protected void processWaitForConnectFinished() {
		if(m_state == State.WAIT_FOR_CONNECT_TO_BEGIN) {
			connect();
		} else {
			System.out.println("Kafka consumer state " + m_state.toString() + ", ignoring WAIT_FOR_CONNECT_FINISHED");
		}
	}

	protected void processDisconnect() {
		if(m_state == State.CONNECTED) {
			m_toggleServiceDependency.activate(false);
			shutdownExecutors();
			startWaitForConnectToBegin(10);			
		} else {
			System.out.println("Kafka consumer state " + m_state.toString() + ", ignoring DISCONNECT");
		}
	}
	
	private void startWaitForConnectToBegin(int delay) {
		m_state = State.WAIT_FOR_CONNECT_TO_BEGIN;
		WaitForConnectToBeginThread waitForConnect = new WaitForConnectToBeginThread(delay, m_queue);
		
		m_waitForConnectToBeginExecutor = Executors.newSingleThreadExecutor();
		m_waitForConnectToBeginExecutor.submit(waitForConnect);
	}
	
	private void shutdownExecutors() {
		
		System.out.println("Shutting down WaitForConnectToBegin executor ...");
		if(m_waitForConnectToBeginExecutor != null) {
			m_waitForConnectToBeginExecutor.shutdownNow();
			try {
				if(!m_waitForConnectToBeginExecutor.awaitTermination(10, TimeUnit.SECONDS)) {
					System.err.println("Timeout waiting for WaitForConnectToBegin to shutdown!");
				}
			} catch (InterruptedException e) {
				System.err.println("WaitForConnectToBegin interrupted during shutdown!");
			}		
			m_waitForConnectToBeginExecutor = null;
		}
	}
	
	private void connect() {
		try {
			m_consumerConnector = kafka.consumer.Consumer.createJavaConsumerConnector(createConsumerConfig());
			m_state = State.CONNECTED;
			m_toggleServiceDependency.activate(true);
			System.out.println("Kafka consumer connected!");
		} catch (Exception e) {
			m_toggleServiceDependency.activate(false);
			System.err.println(e.getMessage());
			startWaitForConnectToBegin(10);
		}
	}
	
	private ConsumerConfig createConsumerConfig() {
		Properties props = new Properties();
		props.put(ZOOKEEPER_CONNECT, m_zookeeperConnect);
		props.put(GROUP_ID, m_groupId);
		props.put(AUTO_COMMIT_INTERVAL, String.valueOf(10 * 1000));
		return new ConsumerConfig(props);
	}
	
	private class WaitForConnectToBeginThread implements Runnable {
		
		private final long m_delay;
		private final BlockingQueue<QueueEvent> m_queue;
		
		public WaitForConnectToBeginThread(long delay, BlockingQueue<QueueEvent> queue) {
			m_delay = delay;
			m_queue = queue;
		}

		@Override
		public void run() {
			try {
				System.out.println("Kafka consumer waiting for " + m_delay + " seconds before attempt to connect.");
				
				Thread.sleep(m_delay * 1000);
				m_queue.put(QueueEvent.WAIT_FOR_CONNECT_FINISHED);
			} catch (InterruptedException e) {
				System.out.println("Kafka consumer waiting for connect to begin was interrupted!");
			}
		}
	}
    
    State getState() {
    	return m_state;
    }

}
