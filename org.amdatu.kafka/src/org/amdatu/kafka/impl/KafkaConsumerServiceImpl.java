package org.amdatu.kafka.impl;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import org.amdatu.kafka.KafkaConsumerService;
import org.amdatu.kafka.impl.KafkaConsumerStateMachine.QueueEvent;
import org.osgi.service.log.LogService;

import kafka.javaapi.consumer.ConsumerConnector;

public class KafkaConsumerServiceImpl implements KafkaConsumerService {
	
	private volatile LogService m_LogService;

	private KafkaConsumerStateMachine m_stateMachine;
	private ExecutorService m_stateMachineExecutorService;
	
	private final BlockingQueue<QueueEvent> m_queue;

	public KafkaConsumerServiceImpl(String zookeeperConnect, String groupId, ToggleServiceDependency toggleServiceDependency) {
		m_queue = new LinkedBlockingQueue<>();
		try {
			m_queue.put(QueueEvent.START);
		} catch (InterruptedException e) {
			m_LogService.log(LogService.LOG_WARNING, "Error while adding START event to queue");
		}
		
		m_stateMachine = new KafkaConsumerStateMachine(zookeeperConnect, groupId, toggleServiceDependency, m_queue);
		m_stateMachineExecutorService = Executors.newSingleThreadExecutor();
		m_stateMachineExecutorService.submit(m_stateMachine);
	}
	
	public void stop() {
		try {
			m_queue.put(QueueEvent.STOP);
		} catch (InterruptedException e) {
			m_LogService.log(LogService.LOG_WARNING, "Error while adding STOP event to queue");
		}
	}

	@Override
	public ConsumerConnector getConsumerConnector() {
		return m_stateMachine.getConsumerConnector();
	}	

}
