package org.amdatu.kafka.impl;

import java.util.Properties;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.log.LogService;

public class Activator extends DependencyActivatorBase {

	@Override
	public void init(BundleContext context, DependencyManager manager) throws Exception {
		Properties producerFactoryProperties = new Properties();
		producerFactoryProperties.put(Constants.SERVICE_PID, KafkaProducerServiceFactory.PID);
		manager.add(createComponent()
					.setInterface(ManagedServiceFactory.class.getName(), producerFactoryProperties)
					.setImplementation(KafkaProducerServiceFactory.class)
					.add(createServiceDependency().setService(LogService.class).setRequired(false)));		
		
		Properties consumerFactoryProperties = new Properties();
		consumerFactoryProperties.put(Constants.SERVICE_PID, KafkaConsumerServiceFactory.PID);
		manager.add(createComponent()
				.setInterface(ManagedServiceFactory.class.getName(), consumerFactoryProperties)
				.setImplementation(KafkaConsumerServiceFactory.class)
				.add(createServiceDependency().setService(LogService.class).setRequired(false)));
	}
	
	@Override
	public void destroy(BundleContext context, DependencyManager manager) {
		
	}

}
