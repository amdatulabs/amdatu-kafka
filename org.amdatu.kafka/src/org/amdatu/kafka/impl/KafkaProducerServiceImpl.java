package org.amdatu.kafka.impl;

import java.util.HashMap;
import java.util.Map;

import org.amdatu.kafka.KafkaProducerService;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;

public class KafkaProducerServiceImpl implements KafkaProducerService {
	
	private static final String BOOTSTRAP_SERVERS = "bootstrap.servers";
	private static final String VALUE_SERIALIZER = "value.serializer";
	private static final String KEY_SERIALIZER = "key.serializer";

	private final Producer<byte[], byte[]> m_producer;
	
	
	public KafkaProducerServiceImpl(String boostrapServers) {
		
		Map<String, Object> properties = new HashMap<>();
		properties.put(BOOTSTRAP_SERVERS, boostrapServers);
		properties.put(VALUE_SERIALIZER, "org.apache.kafka.common.serialization.ByteArraySerializer");
		properties.put(KEY_SERIALIZER, "org.apache.kafka.common.serialization.ByteArraySerializer");

		m_producer = new KafkaProducer<byte[], byte[]>(properties);
	}

	@Override
	public Producer<byte[], byte[]> getProducer() {
		return m_producer;
	}
	
	public void close() {
		m_producer.close();
	}
	
}
