package org.amdatu.kafka.impl;

import org.apache.felix.dm.context.AbstractDependency;
import org.apache.felix.dm.context.DependencyContext;
import org.apache.felix.dm.context.Event;
import org.apache.felix.dm.context.EventType;

/**
 * Copy/paste from com.amdatu.informationgrid.base.
 * 
 * Dependency that allows a component to programmatically choose the moment of enabling itself.
 * In other words, a component C that declares to have a dependency of this type will not be activated unless
 * this dependency is also active, which can only happen programmatically.<br/>
 * All other required dependencies of the component need to be satisfied too, so calling 'activate' on this
 * dependency does not ensure activation of the component, but it allows it to become active.
 */
public class ToggleServiceDependency extends AbstractDependency<ToggleServiceDependency> {
	
	public ToggleServiceDependency() {
		super.setRequired(true);
	}
	
	public ToggleServiceDependency(ToggleServiceDependency other) {
		super(other);
	}
	
	public void activate(boolean active) {
		if(isStarted()) {
			m_component.handleEvent(this, active ? EventType.ADDED : EventType.REMOVED, new Event(active));
		}
	}

	@Override
	public DependencyContext createCopy() {
		return new ToggleServiceDependency(this);
	}

	@Override
	public Class<?> getAutoConfigType() {
		return null; // not required
	}

	@Override
	public String getSimpleName() {
		return "" + isAvailable();
	}

	@Override
	public String getType() {
		return "toggle";
	}

}
