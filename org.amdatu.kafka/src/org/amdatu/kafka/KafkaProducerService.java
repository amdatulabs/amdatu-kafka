package org.amdatu.kafka;

import org.apache.kafka.clients.producer.Producer;

public interface KafkaProducerService {
	Producer<byte[], byte[]> getProducer();
}
