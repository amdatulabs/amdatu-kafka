package org.amdatu.kafka;

import kafka.javaapi.consumer.ConsumerConnector;

public interface KafkaConsumerService {
	ConsumerConnector getConsumerConnector();
}
