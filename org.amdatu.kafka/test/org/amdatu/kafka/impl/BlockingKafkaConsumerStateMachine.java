package org.amdatu.kafka.impl;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;

public class BlockingKafkaConsumerStateMachine extends
		KafkaConsumerStateMachine {

	private CountDownLatch m_latch;
	
	public BlockingKafkaConsumerStateMachine(
			String zookeeperConnect,
			String groupId,
			ToggleServiceDependency toggleServiceDependency,
			BlockingQueue<QueueEvent> queue) {
		super(zookeeperConnect, groupId, toggleServiceDependency, queue);
	}
	
	@Override
	protected void processStart() {
		super.processStart();
		m_latch.countDown();		
	}
	
	@Override
	protected void processWaitForConnectFinished() {
		super.processWaitForConnectFinished();
		m_latch.countDown();
	}
	
	@Override
	protected void processDisconnect() {
		super.processDisconnect();
		m_latch.countDown();
	}
		
	@Override
	protected void processStop() {
		super.processStop();
		m_latch.countDown();
	}
	
	public void setLatch(CountDownLatch latch) {
		m_latch = latch;
	}
	
}
