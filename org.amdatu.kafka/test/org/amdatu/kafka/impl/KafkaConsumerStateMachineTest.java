package org.amdatu.kafka.impl;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.amdatu.kafka.impl.KafkaConsumerStateMachine.QueueEvent;
import org.amdatu.kafka.impl.KafkaConsumerStateMachine.State;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class KafkaConsumerStateMachineTest {
	
	private static final String ZOOKEEPER_CONNECT = "localhost:9999";
	private static final String GROUP_ID = "test.group.id";
	
	@Mock
	private ToggleServiceDependency m_toggleServiceDependency; 
	
	private BlockingQueue<QueueEvent> m_queue;
	
	@Before
	public void setUp() {
		m_queue = new LinkedBlockingQueue<QueueEvent>();
	}
	
	@After
	public void tearDown() {
		
	}
	
	@Test
	public void testStartConsumer() throws IOException, InterruptedException {
		EmbeddedZookeeper embeddedZookeeper = new EmbeddedZookeeper(9999);		
		embeddedZookeeper.startup();
		
		ExecutorService stateMachineExecutor = Executors.newSingleThreadExecutor();
		BlockingKafkaConsumerStateMachine stateMachine = new BlockingKafkaConsumerStateMachine(ZOOKEEPER_CONNECT, GROUP_ID, m_toggleServiceDependency, m_queue);
		stateMachineExecutor.submit(stateMachine);
		
		
		CountDownLatch latch = new CountDownLatch(2);
		stateMachine.setLatch(latch);
		
		start();
		
		assertTrue(latch.await(30, TimeUnit.SECONDS));
		assertTrue(stateMachine.getState().equals(State.CONNECTED));
		
		embeddedZookeeper.shutdown();
		stateMachineExecutor.shutdownNow();
		
	}
	
	@Test
	public void testZookeeperDown() throws IOException, InterruptedException {
		
		ExecutorService stateMachineExecutor = Executors.newSingleThreadExecutor();
		BlockingKafkaConsumerStateMachine stateMachine = new BlockingKafkaConsumerStateMachine(ZOOKEEPER_CONNECT, GROUP_ID, m_toggleServiceDependency, m_queue);
		
		CountDownLatch latch = new CountDownLatch(2);
		stateMachine.setLatch(latch);

		stateMachineExecutor.submit(stateMachine);
		
		start();
		
		assertTrue(latch.await(30, TimeUnit.SECONDS));
		assertTrue(stateMachine.getState().equals(State.WAIT_FOR_CONNECT_TO_BEGIN));
		
		stateMachineExecutor.shutdownNow();
	}
	
	@Test
	public void testStopConsumer() throws IOException, InterruptedException {
		EmbeddedZookeeper embeddedZookeeper = new EmbeddedZookeeper(9999);		
		embeddedZookeeper.startup();
		
		ExecutorService stateMachineExecutor = Executors.newSingleThreadExecutor();
		BlockingKafkaConsumerStateMachine stateMachine = new BlockingKafkaConsumerStateMachine(ZOOKEEPER_CONNECT, GROUP_ID, m_toggleServiceDependency, m_queue);
		stateMachineExecutor.submit(stateMachine);
		
		
		CountDownLatch latch = new CountDownLatch(2);
		stateMachine.setLatch(latch);
		
		start();
		
		assertTrue(latch.await(30, TimeUnit.SECONDS));
		assertTrue(stateMachine.getState().equals(State.CONNECTED));
		
		latch = new CountDownLatch(1);
		stateMachine.setLatch(latch);
		stop();
		assertTrue(latch.await(30, TimeUnit.SECONDS));
		assertTrue(stateMachine.getState().equals(State.NOT_CONNECTED));
		
		embeddedZookeeper.shutdown();
		stateMachineExecutor.shutdownNow();
	}
	
	private void start() throws InterruptedException {
		m_queue.put(QueueEvent.START);
	}
	
	private void stop() throws InterruptedException {
		m_queue.put(QueueEvent.STOP);
	}
	
}
